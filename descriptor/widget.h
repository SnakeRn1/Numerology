#ifndef WIDGET_H
#define WIDGET_H
#include <QWidget>
#include <QScrollBar>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QGraphicsEffect>
#include "descriptor.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private:
    Ui::Widget *ui;
    Descriptor des;
    QPoint d;
    QGraphicsDropShadowEffect *eff;
    QGraphicsDropShadowEffect *txt_eff;
    int col = 0;

protected:
    void on_but_clicked();
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
};
#endif // WIDGET_H
