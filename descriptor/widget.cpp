#include "widget.h"
#include "ui_widget.h"
#include <iostream>


Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    setWindowFlag(Qt::WindowType::FramelessWindowHint);
    setWindowFlag(Qt::WindowType::WindowStaysOnTopHint);
    setAttribute(Qt::WidgetAttribute::WA_TranslucentBackground);
    eff = new QGraphicsDropShadowEffect(this);
    txt_eff = new QGraphicsDropShadowEffect(this);
    eff->setOffset(0);
    eff->setBlurRadius(20);
    eff->setColor(Qt::GlobalColor::magenta);
    ui->widget->setGraphicsEffect(eff);
    txt_eff->setColor(Qt::GlobalColor::white);
    txt_eff->setBlurRadius(15);
    txt_eff->setOffset(0);
    ui->txt->setGraphicsEffect(txt_eff);

    show();
    on_but_clicked();
}

Widget::~Widget()
{
    delete eff;
    delete ui;
}

void Widget::on_but_clicked()
{
    ui->txt->setText("____Подробное описание квадрата Пифагора (психоматрицы)\r\n"
                     "____Хоружий Руслан, 15.12.2020г.\r\n"
                     "____Компания: SnakeR Soft\r\n"
                     "____Продукт: Numerology Decomposition\r\n"
                     "____Версия: 1.0.6\r\n"
                     "\r\n"
                     "*    Правая кнопка мыши - выход\r\n"
                     "*    Нажать на колесико - цвет окна\r\n");
    for (int num(1);num<18;++num)
    {
        int gate = 0;
        if( (num == 1)||(num > 9) ) gate = 6;
        else if ( (num == 2) || (num == 3) ) gate = 4;
        else gate = 3;
        ui->txt->setTextColor(Qt::GlobalColor::darkBlue);
        ui->txt->append("\r\n" + des.get_cell_description(num) + "\r\n");
        for (int sum(0); sum<=gate; ++sum)
        {
            ui->txt->setTextColor(Qt::GlobalColor::blue);
            ui->txt->append(des.get_pre_total_description(num,sum));
            ui->txt->setTextColor(Qt::GlobalColor::black);
            ui->txt->append(des.get_total_cell_description(num,sum)+"\r\n");
        }
    }
    for (int num(1);num<10;++num)
    {
        ui->txt->setTextColor(Qt::GlobalColor::blue);
        ui->txt->append("Число судьбы "+QString::number(num)+" означает:\r\n");
        ui->txt->setTextColor(Qt::GlobalColor::black);
        ui->txt->append(des.get_destiny_number_description(num)+"\r\n");
    }
    ui->txt->verticalScrollBar()->setValue(0);
}

void Widget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::MouseButton::LeftButton) d = event->pos();
    if (event->button() == Qt::MouseButton::RightButton) close();
    if (event->button() == Qt::MouseButton::MidButton)
    {
        if(col == 0) eff->setColor(Qt::GlobalColor::red);
        if(col == 1) eff->setColor(Qt::GlobalColor::blue);
        if(col == 2) eff->setColor(Qt::GlobalColor::cyan);
        if(col == 3) eff->setColor(Qt::GlobalColor::gray);
        if(col == 4) eff->setColor(Qt::GlobalColor::black);
        if(col == 5) eff->setColor(Qt::GlobalColor::green);
        if(col == 6) eff->setColor(Qt::GlobalColor::white);
        if(col == 7) eff->setColor(Qt::GlobalColor::yellow);
        if(col == 8) eff->setColor(Qt::GlobalColor::magenta);
        if(col == 9) eff->setColor(Qt::GlobalColor::transparent);
        if(col++ == 10) col = 0;
    }
}

void Widget::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::MouseButton::LeftButton) move(event->globalPos()-d);
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key::Key_Escape) close();
}
