#include "calc.h"
#include "ui_form.h"

CalcDate::CalcDate(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::CalcDate)
{
    ui->setupUi(this);
    ui->calendarWidget->setCurrentPage(ui->years->value(),ui->months->currentIndex()+1);
    mat = new MatrixPifagor();
    html = ui->result_main_text->toHtml();
}

CalcDate::~CalcDate()
{
    delete mat;
    delete ui;
}

void CalcDate::print_symbol_txt(int num)
{
    ui->result_main_text->setTextColor(QColor(Qt::GlobalColor::darkMagenta));
    ui->result_main_text->append(mat->descriptor.get_cell_description(num));
    ui->result_main_text->setTextColor(QColor(Qt::GlobalColor::darkGreen));
    ui->result_main_text->append(mat->descriptor.get_pre_total_description(num));
    ui->result_main_text->setTextColor(QColor(Qt::GlobalColor::black));
    ui->result_main_text->append(mat->descriptor.get_total_cell_description(num)+"\r\n");
}

void CalcDate::on_button_calculate_clicked()
{
    mat->hide();
    if(date_now.isNull())
    {
        ui->label_date_select->setText("Дата не выбрана!");
    }
    else
    {
        int d = date_now.day();
        int m = date_now.month();
        int y = date_now.year();

        QString d_s = QString::number(d).rightJustified(2,'0');
        QString m_s = QString::number(m).rightJustified(2,'0');
        QString y_s = QString::number(y).rightJustified(4,'0');

        int first = 0;
        int second = 0;
        int third = 0;
        int fourth = 0;

        int dd = d_s.mid(0,1).toInt() + d_s.mid(1,1).toInt();
        int mm = m_s.mid(0,1).toInt() + m_s.mid(1,1).toInt();
        int yyyy = y_s.mid(0,1).toInt() + y_s.mid(1,1).toInt() + y_s.mid(2,1).toInt() + y_s.mid(3,1).toInt();

        first = dd+mm+yyyy;

        QString first_txt = QString::number(first).rightJustified(2,'0');
        second = first_txt.mid(0,1).toInt() + first_txt.mid(1,1).toInt();

        if(d_s.mid(0,1).toInt()) third = d_s.mid(0,1).toInt()*2;
        else third = d_s.mid(1,1).toInt()*2;
        third = first - third;

        QString fourth_txt = QString::number(third).rightJustified(2,'0');
        fourth = fourth_txt.mid(0,1).toInt() + fourth_txt.mid(1,1).toInt();

        QString txt1 = QString::number(first).rightJustified(2,'0');
        QString txt2 = QString::number(second).rightJustified(2,'0');
        QString txt3 = QString::number(third).rightJustified(2,'0');
        QString txt4 = QString::number(fourth).rightJustified(2,'0');

        ui->tableWidget->item(0,0)->setText(txt1);
        ui->tableWidget->item(0,1)->setText(txt2);
        ui->tableWidget->item(0,2)->setText(txt3);
        ui->tableWidget->item(0,3)->setText(txt4);

        ui->result_main_text->clear();
        result_string_number.clear();

        QString lives_str (txt1+txt2+txt3+txt4);
        int how_many = 0;
        if (lives_str.size() == 8)
        {
            for (int pp=0;pp<8;++pp)
            {
                how_many += lives_str.mid(pp,1).toInt();
            }
            if(how_many>15)
            {
                how_many = how_many/10 + how_many%10;
            }
            if(how_many>15)
            {
                how_many = how_many/10 + how_many%10;
            }
        }

        QString digit_str (d_s+m_s+y_s);
        int digit = 0;

        if (digit_str.size() == 8)
        {
            for (int pp=0;pp<8;++pp)
            {
                digit += digit_str.mid(pp,1).toInt();
            }
            if(digit>=10)
            {
                digit = digit/10 + digit%10;
            }
            if(digit>=10)
            {
                digit = digit/10 + digit%10;
            }
        }

        ui->result_main_text->setTextColor(QColor(Qt::GlobalColor::black));
        ui->result_main_text->append("Краткая расшифровка ячеек квадрата пифагора на дату: " + date_now.toString(Qt::DateFormat::DefaultLocaleLongDate));
        ui->result_main_text->setTextColor(QColor(Qt::GlobalColor::darkGray));
        ui->result_main_text->append("\r\n____В индивидуальном числе, полученном в результате расчета, содержится определенное количество цифр 0,1,2 и т.д. Ячейки квадрата, каждая из которых отвечает за одну цифру, заполняются количеством соответствующих цифр в индивидуальном числе. К примеру, если цифра 2 в инд.числе встретилась три раза, то ячейка, соответствующая номеру 2 будет содержать в себе ряд из трех цифр '222'. Напротив, ячейка, не содержащая в себе ни одного знака говорит о том, что в инд. числе данной цифры не нашлось. Каждая ячейка характеризует определенные свойства личности. Количество цифр в ячейке означает интенсивность проявления того или иного качества человека.\r\n");

        result_string_number = lives_str+"\r\n"+digit_str;

        ui->label_result_code->setText(result_string_number);

        mat->create_matrix(result_string_number);

        for (int num = 0; num<18; ++num)
        {
            if(num) print_symbol_txt(num);
        }

        ui->result_main_text->setTextColor(QColor(Qt::GlobalColor::darkMagenta));
        ui->result_main_text->append("____Число судьбы вычисляется по дате рождения путем сложения всех цифр даты рождения и последующего приведения полученного числа к одиночной цифре. Характеризует кармическое влияние на жизнь человека, определяя основной долг. Проявляется интенсивно к 35-ти годам.");
        ui->result_main_text->setTextColor(QColor(Qt::GlobalColor::darkGreen));
        ui->result_main_text->append("____Итоговое число по дате рождения (число судьбы): " + QString::number(digit));
        ui->result_main_text->setTextColor(QColor(Qt::GlobalColor::black));
        ui->result_main_text->append(mat->descriptor.get_destiny_number_description(digit));
        if(digit%2) ui->result_main_text->append("____Люди с нечетным числом судьбы устремляются к изменениям в любой сфере жизни, пытаясь изменить мир к лучшему. Упрямые и решительные - они готовы взять на себя лидерство. В этом им помогает внимательность к мелочам и тонкое психологическое чутье.");
        else ui->result_main_text->append("____Люди с четным числом приверженцы красивого, гармоничного мира. Если мир вокруг них самый грязный - он станет ярким и красочным. Многие могут подумать, что это ребячество и идеализм, но отстаивание своей позиции и мужество в достижении целей говорит о вполне серьезных планах поиска мудрости и правды во всем. Люди с четным числом судьбы любвеобильны.");

        ui->result_main_text->setTextColor(QColor(Qt::GlobalColor::darkMagenta));
        ui->result_main_text->append("\r\n____В некоторых источниках указано, что согласно Пифагорейскому учению, человек проживает несколько жизней, перевоплащаясь при каждом рождении. Максимальное количество таких реинкарнаций - 15. Выяснить текущую реинкарнацию позволяет сложение всех цифр из четырех чисел, полученных после преобразования даты рождения. Если число больше 15-ти, его цифры вновь суммируются до тех пор, пока сумма цифр не станет находиться в диапазоне от 1 до 15.");
        ui->result_main_text->setTextColor(QColor(Qt::GlobalColor::darkGreen));
        ui->result_main_text->append("____Номер текущей жизни (реинкарнация): " + QString::number(how_many) + "\r\n");

        ui->result_main_text->verticalScrollBar()->setValue(0);
        ui->pushButton->setEnabled(true);
        ui->lineEdit->setEnabled(true);
    }
}

void CalcDate::closeEvent(QCloseEvent *event)
{
    event->accept();
    mat->close();
}

void CalcDate::all_fields_reset()
{
    mat->hide();
    ui->result_main_text->clear();
    ui->result_main_text->setHtml(html);
    ui->lineEdit->clear();
    ui->lineEdit->setEnabled(false);
    ui->pushButton->setEnabled(false);
    result_string_number.clear();
    ui->label_date_select->setText("Выберите необходимую дату");
    ui->label_result_code->clear();
}

void CalcDate::on_years_valueChanged(int arg1)
{
    all_fields_reset();
    if (arg1 != ui->calendarWidget->yearShown()) ui->calendarWidget->setCurrentPage(arg1,ui->months->currentIndex()+1);
}

void CalcDate::on_months_currentIndexChanged(int index)
{
    all_fields_reset();
    index++;
    if (index != ui->calendarWidget->monthShown()) ui->calendarWidget->setCurrentPage(ui->years->value(),index);
}

void CalcDate::on_calendarWidget_clicked(const QDate &date)
{
    all_fields_reset();
    ui->label_date_select->setText("Выбрано: "+date.toString(Qt::DefaultLocaleLongDate));
    date_now = date;
}

void CalcDate::on_button_pifagor_clicked()
{
    if(date_now.isNull())
    {
        ui->label_date_select->setText("Дата не выбрана!");
    }
    else
    {
        if(result_string_number.isEmpty())
        {
            on_button_calculate_clicked();
            mat->create_matrix(result_string_number);
            mat->show();
        }
        else
        {
            mat->create_matrix(result_string_number);
            mat->show();
        }
    }
}

void CalcDate::on_calendarWidget_currentPageChanged(int year, int month)
{
    month--;
    if(year != ui->years->value())ui->years->setValue(year);
    if(month != ui->months->currentIndex())ui->months->setCurrentIndex(month);
}

void CalcDate::on_pushButton_clicked()
{
    QString name = ui->lineEdit->text();
    if (name.isEmpty()) name = "по дате";
    QString filename = "Нумерологический отчет " + name + " (" + date_now.toString(Qt::DefaultLocaleLongDate) + ")";
    QString absolute = QFileDialog::getSaveFileName(this,"Сохранить отчет",filename,"text(*.txt)");
    QFile file(absolute);
    if(file.open(QIODevice::WriteOnly))
    {
        QTextStream stream(&file);
        QString txt;
        if(name == "по дате") txt = "Имя человека не было указано.\n" + ui->result_main_text->toPlainText();
        else txt = "Имя человека, в отношении которого производился расчет: " + name + "\n" + ui->result_main_text->toPlainText();
        txt.replace("\n","\r\n");
        stream<<txt;
    }
    file.close();
}
