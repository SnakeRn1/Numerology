#include "matrixpifagor.h"
#include "ui_matrixpifagor.h"

MatrixPifagor::MatrixPifagor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MatrixPifagor)
{
    ui->setupUi(this);
}

MatrixPifagor::~MatrixPifagor()
{
    delete ui;
}

void MatrixPifagor::create_matrix(QString initial)
{
    for (int num = 0; num<10; ++num)
    {
        int sum = initial.count(QString::number(num));
        QString txt;
        if (!sum) txt.append("-");
        else
        {
            for(int i = 0; i<sum; ++i)
            {
                txt.append(QString::number(num));
            }
        }
        switch (num)
        {
        case 1:
            descriptor.val.m1 = sum;
            ui->m1->setText(txt);
            break;
        case 2 :
            descriptor.val.m2 = sum;
            ui->m2->setText(txt);
            break;
        case 3:
            descriptor.val.m3 = sum;
            ui->m3->setText(txt);
            break;
        case 4 :
            descriptor.val.m4 = sum;
            ui->m4->setText(txt);
            break;
        case 5:
            descriptor.val.m5 = sum;
            ui->m5->setText(txt);
            break;
        case 6 :
            descriptor.val.m6 = sum;
            ui->m6->setText(txt);
            break;
        case 7:
            descriptor.val.m7 = sum;
            ui->m7->setText(txt);
            break;
        case 8 :
            descriptor.val.m8 = sum;
            ui->m8->setText(txt);
            break;
        case 9:
            descriptor.val.m9 = sum;
            ui->m9->setText(txt);
            break;
        }
    }

    descriptor.val.c1 = descriptor.val.m1 + descriptor.val.m2 + descriptor.val.m3;
    ui->c1->setText("Самооценка\r\n" + QString::number(descriptor.val.c1));

    descriptor.val.c2 = descriptor.val.m4 + descriptor.val.m5 + descriptor.val.m6;
    ui->c2->setText("Работа\r\n" + QString::number(descriptor.val.c2));

    descriptor.val.c3 = descriptor.val.m7 + descriptor.val.m8 + descriptor.val.m9;
    ui->c3->setText("Талант\r\n" + QString::number(descriptor.val.c3));

    descriptor.val.r1 = descriptor.val.m1 + descriptor.val.m4 + descriptor.val.m7;
    ui->r1->setText("Цель\r\n" + QString::number(descriptor.val.r1));

    descriptor.val.r2 = descriptor.val.m2 + descriptor.val.m5 + descriptor.val.m8;
    ui->r2->setText("Семья\r\n" + QString::number(descriptor.val.r2));

    descriptor.val.r3 = descriptor.val.m3 + descriptor.val.m6 + descriptor.val.m9;
    ui->r3->setText("Привычки\r\n" + QString::number(descriptor.val.r3));

    descriptor.val.d1 = descriptor.val.m1 + descriptor.val.m5 + descriptor.val.m9;
    ui->d1->setText("Дух\r\n" + QString::number(descriptor.val.d1));

    descriptor.val.d2 = descriptor.val.m3 + descriptor.val.m5 + descriptor.val.m7;
    ui->d2->setText("Тело\r\n" + QString::number(descriptor.val.d2));

    ui->textBrowser->setText("Нажмите на интересующую характеристику для вывода подробностей.");
}

void MatrixPifagor::on_m1_clicked()
{
    unchceck_all_buttons();
    ui->m1->setChecked(true);
    print_fields(1);
}

void MatrixPifagor::on_m2_clicked()
{
    unchceck_all_buttons();
    ui->m2->setChecked(true);
    print_fields(2);
}

void MatrixPifagor::on_m3_clicked()
{
    unchceck_all_buttons();
    ui->m3->setChecked(true);
    print_fields(3);
}

void MatrixPifagor::on_m4_clicked()
{
    unchceck_all_buttons();
    ui->m4->setChecked(true);
    print_fields(4);
}

void MatrixPifagor::on_m5_clicked()
{
    unchceck_all_buttons();
    ui->m5->setChecked(true);
    print_fields(5);
}

void MatrixPifagor::on_m6_clicked()
{
    unchceck_all_buttons();
    ui->m6->setChecked(true);
    print_fields(6);
}

void MatrixPifagor::on_m7_clicked()
{
    unchceck_all_buttons();
    ui->m7->setChecked(true);
    print_fields(7);
}

void MatrixPifagor::on_m8_clicked()
{
    unchceck_all_buttons();
    ui->m8->setChecked(true);
    print_fields(8);
}

void MatrixPifagor::on_m9_clicked()
{
    unchceck_all_buttons();
    ui->m9->setChecked(true);
    print_fields(9);
}

void MatrixPifagor::on_c1_clicked()
{
    unchceck_all_buttons();
    ui->c1->setChecked(true);
    ui->m1->setChecked(true);
    ui->m2->setChecked(true);
    ui->m3->setChecked(true);
    print_fields(10);
}

void MatrixPifagor::on_c2_clicked()
{
    unchceck_all_buttons();
    ui->c2->setChecked(true);
    ui->m4->setChecked(true);
    ui->m5->setChecked(true);
    ui->m6->setChecked(true);
    print_fields(11);
}

void MatrixPifagor::on_c3_clicked()
{
    unchceck_all_buttons();
    ui->c3->setChecked(true);
    ui->m7->setChecked(true);
    ui->m8->setChecked(true);
    ui->m9->setChecked(true);
    print_fields(12);
}

void MatrixPifagor::on_r1_clicked()
{
    unchceck_all_buttons();
    ui->r1->setChecked(true);
    ui->m1->setChecked(true);
    ui->m4->setChecked(true);
    ui->m7->setChecked(true);
    print_fields(13);
}

void MatrixPifagor::on_r2_clicked()
{
    unchceck_all_buttons();
    ui->r2->setChecked(true);
    ui->m2->setChecked(true);
    ui->m5->setChecked(true);
    ui->m8->setChecked(true);
    print_fields(14);
}

void MatrixPifagor::on_r3_clicked()
{
    unchceck_all_buttons();
    ui->r3->setChecked(true);
    ui->m3->setChecked(true);
    ui->m6->setChecked(true);
    ui->m9->setChecked(true);
    print_fields(15);
}

void MatrixPifagor::on_d1_clicked()
{
    unchceck_all_buttons();
    ui->d1->setChecked(true);
    ui->m1->setChecked(true);
    ui->m5->setChecked(true);
    ui->m9->setChecked(true);
    print_fields(16);
}

void MatrixPifagor::on_d2_clicked()
{
    unchceck_all_buttons();
    ui->d2->setChecked(true);
    ui->m3->setChecked(true);
    ui->m5->setChecked(true);
    ui->m7->setChecked(true);
    print_fields(17);
}

void MatrixPifagor::print_fields(int num)
{
    ui->textBrowser->setTextColor(QColor(Qt::GlobalColor::darkMagenta));
    ui->textBrowser->setText(descriptor.get_cell_description(num));
    ui->textBrowser->setTextColor(QColor(Qt::GlobalColor::darkGreen));
    ui->textBrowser->append(descriptor.get_pre_total_description(num));
    ui->textBrowser->setTextColor(QColor(Qt::GlobalColor::black));
    ui->textBrowser->append(descriptor.get_total_cell_description(num));
    ui->textBrowser->verticalScrollBar()->setValue(0);
}

void MatrixPifagor::unchceck_all_buttons()
{
    ui->m1->setChecked(false);
    ui->m2->setChecked(false);
    ui->m3->setChecked(false);
    ui->m4->setChecked(false);
    ui->m5->setChecked(false);
    ui->m6->setChecked(false);
    ui->m7->setChecked(false);
    ui->m8->setChecked(false);
    ui->m9->setChecked(false);
    ui->c1->setChecked(false);
    ui->c2->setChecked(false);
    ui->c3->setChecked(false);
    ui->r1->setChecked(false);
    ui->r2->setChecked(false);
    ui->r3->setChecked(false);
    ui->d1->setChecked(false);
    ui->d2->setChecked(false);
}

void MatrixPifagor::hideEvent(QHideEvent *event)
{
    event->accept();
    unchceck_all_buttons();
}
