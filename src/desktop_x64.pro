QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = ../../num_help_x64/num_help_x64
VERSION = 1.0.6.0
RC_ICONS += ico.ico
QMAKE_TARGET_COMPANY = "SnakeR Soft"
QMAKE_TARGET_PRODUCT = "Numerology Helper"
QMAKE_TARGET_DESCRIPTION = "Numerology Helper"
QMAKE_TARGET_COPYRIGHT = "Copyright © 2020 Horuzhiy Ruslan"
CONFIG += c++11
DEFINES += QT_DEPRECATED_WARNINGS
SOURCES += descriptor.cpp main.cpp calc.cpp matrixpifagor.cpp
HEADERS += calc.h descriptor.h matrixpifagor.h
FORMS += form.ui matrixpifagor.ui
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
