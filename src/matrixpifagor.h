#ifndef MATRIXPIFAGOR_H
#define MATRIXPIFAGOR_H

#include <QWidget>
#include <QCloseEvent>
#include <QScrollBar>
#include <QHideEvent>
#include "descriptor.h"

namespace Ui {
class MatrixPifagor;
}

class MatrixPifagor : public QWidget
{
    Q_OBJECT

public:
    explicit MatrixPifagor(QWidget *parent = nullptr);
    ~MatrixPifagor();
    void create_matrix(QString initial);
    Descriptor descriptor;

private slots:
    void on_m1_clicked();
    void on_m2_clicked();
    void on_m3_clicked();
    void on_m4_clicked();
    void on_m5_clicked();
    void on_m6_clicked();
    void on_m7_clicked();
    void on_m8_clicked();
    void on_m9_clicked();
    void on_c1_clicked();
    void on_c2_clicked();
    void on_c3_clicked();
    void on_r1_clicked();
    void on_r2_clicked();
    void on_r3_clicked();
    void on_d1_clicked();
    void on_d2_clicked();
    void unchceck_all_buttons();

private:
    Ui::MatrixPifagor *ui;

    void print_fields(int num);

protected:
    void hideEvent(QHideEvent *event);
};

#endif // MATRIXPIFAGOR_H
