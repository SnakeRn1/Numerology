#ifndef DESCRIPTOR_H
#define DESCRIPTOR_H
#include <QString>

class Descriptor
{
public:
    Descriptor();
    QString get_destiny_number_description(int num);
    QString get_cell_description(int num);
    QString get_total_cell_description(int num, int sum = -1);
    QString get_pre_total_description(int num, int sum = -1);
    union
    {
        struct {int m0,m1,m2,m3,m4,m5,m6,m7,m8,m9,c1,c2,c3,r1,r2,r3,d1,d2;} val;
        int all[18];
    };
};

#endif // DESCRIPTOR_H
