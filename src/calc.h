#ifndef CALCDATE_H
#define CALCDATE_H

#include <QWidget>
#include <QDate>
#include <QTextStream>
#include <QFileDialog>
#include "matrixpifagor.h"
#include "ui_matrixpifagor.h"

QT_BEGIN_NAMESPACE
namespace Ui { class CalcDate; }
QT_END_NAMESPACE

class CalcDate : public QWidget
{
    Q_OBJECT

public:
    CalcDate(QWidget *parent = nullptr);
    ~CalcDate();
private slots:
    void on_calendarWidget_clicked(const QDate &date);
    void on_years_valueChanged(int arg1);
    void on_months_currentIndexChanged(int index);
    void on_button_calculate_clicked();
    void on_button_pifagor_clicked();
    void on_calendarWidget_currentPageChanged(int year, int month);
    void on_pushButton_clicked();

private:
    QDate date_now;
    Ui::CalcDate *ui;
    void print_symbol_txt(int num);
    MatrixPifagor* mat;
    QString result_string_number;
    QString html;
    void all_fields_reset();
protected:
    void closeEvent(QCloseEvent *event);
};
#endif // CALCDATE_H
